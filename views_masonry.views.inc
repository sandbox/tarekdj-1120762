<?php
 
 
/**
 * Implementation of hook_views_plugins
 */
function views_masonry_views_plugins() {
  return array(
    'style' => array(
      'views_masonry' => array(
        'title' => t('Views Masonry'),
        'theme' => 'views_view_masonry',
        'help' => t('Provide a Masonry display style for Views.'),
        'handler' => 'views_masonry_style_plugin',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}