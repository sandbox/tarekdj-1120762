<?php
/**
  * Implementation of views_plugin_style().
  */
class views_masonry_style_plugin extends views_plugin_style {

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['column_width'] = array(
      '#type' => 'textfield',
      '#title' => t('Column width'),
      '#default_value' => 50,
      '#description' => t('Width in pixels of 1 column of your grid. default: outer width of the first floated element.'),
    );

  }
}