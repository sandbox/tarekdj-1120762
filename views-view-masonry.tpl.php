<?php // $Id: ?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<div id="tumblelog">
<?php
 foreach ($rows as $key => $row) {
	// extract the nid from the view
	$nid=$view->result[$key]->nid;
	// build column width class
	$div_class=masonry_class($nid);
    print '<div class="box '.$div_class.'">'. $row . '</div>';
}
?>
 <?php print $script ?> 
</div>