<?php
// $Id$

function masonry_admin_settings(){
	$options = node_get_types('names');
	
	$form['masonry_node_types'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Content types to use with masonry'),
		'#options' => $options,
		'#default_value' => variable_get('masonry_node_types', array('page')),
		'#description' => t('A text field will be available on these content type to set a specific masonry column width'),
	);

	return system_settings_form($form);
}